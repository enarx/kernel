FROM debian:bookworm

RUN apt update \
 && apt install --no-install-recommends -y wget unzip tar bzip2 ca-certificates \
 && rm -rf /var/lib/apt/lists/*

# Download and unpack our kernel
RUN wget "https://nightly.link/enarx/linux/workflows/build/sgx-snp-v2/linux.tar.bz2.zip" && \
    unzip linux.tar.bz2.zip && \
    tar -xj -C / -f linux.tar.bz2 && \
    rm linux.tar.bz2*

# Download the latest firmware
RUN mkdir -p /
RUN wget -O /amd_sev_fam19h_model0xh.sbin "https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/amd/amd_sev_fam19h_model0xh.sbin"


FROM debian:bookworm

RUN sed -i 's| main| main contrib non-free|' /etc/apt/sources.list
RUN apt update \
 && apt install --no-install-recommends -y initramfs-tools \
 && ln -sf /bin/true /usr/sbin/update-initramfs \
 && apt install --no-install-recommends -y firmware-linux \
 && apt install -y systemd \
 && rm -rf /var/lib/apt/lists/*

COPY --from=0 /boot /boot
COPY --from=0 /lib/modules/ /lib/modules/
COPY --from=0 /amd_sev_fam19h_model0xh.sbin /lib/firmware/amd/

RUN cd /boot; ln -s vmlinuz* wyrcan.kernel
RUN ln -s /bin/systemd /init
